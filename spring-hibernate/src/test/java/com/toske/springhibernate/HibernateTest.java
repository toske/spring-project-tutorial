package com.toske.springhibernate;

import com.toske.springhibernate.model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

public class HibernateTest {

    private static final SessionFactory sessionFactory = new  Configuration()
                                                .configure()
                                                .addAnnotatedClass(Student.class)
                                                .buildSessionFactory();

    private Session session;

    @After
    public void close() {
        if (sessionFactory.isOpen()) {
            sessionFactory.close();
        }
    }

    @Ignore
    @Test
    public void createStudentObjectAndSaveIntoDB() {

        Student theStudent = new Student("Milos", "Tosic", "toske@mail.com");

        try {
            session = sessionFactory.getCurrentSession();

            session.beginTransaction();

            session.save(theStudent);

            session.getTransaction().commit();
        } finally {
            sessionFactory.close();
        }
    }

    @Ignore
    @Test
    public void testPrimaryKeyIncrement() {

        Student student1 = new Student("Milena", "Milosevic", "milena@mail.com");
        Student student2 = new Student("Bojana", "Tosic", "bojana@mail.com");
        Student student3 = new Student("Tamara", "Milosevic", "tamara@mail.com");

        try {
            session = sessionFactory.getCurrentSession();

            session.beginTransaction();

            session.save(student1);
            session.save(student2);
            session.save(student3);

            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sessionFactory.close();
        }
    }
    @Ignore
    @Test
    public void testReadingObjectFromDB() {

        System.out.println("Creating new object...");
        Student theStudent = new Student("Nikola", "Milosevic", "nikola@mail.com");

        try {
            session = sessionFactory.getCurrentSession();

            session.beginTransaction();

            System.out.println("Saving the student...");
            System.out.println(theStudent);
            session.save(theStudent);

            session.getTransaction().commit();

            System.out.println("Done!");

            session = sessionFactory.getCurrentSession();

            session.beginTransaction();

            Student myStudent = session.get(Student.class, theStudent.getId());

            System.out.println("Get completed: " + myStudent);

            session.getTransaction().commit();

            System.out.println("Done reading from DB");
        } finally {
            sessionFactory.close();
        }
    }

    @Ignore
    @Test
    public void testReadingObjectFromDB2() {

        Student student;

        try {
            session = sessionFactory.getCurrentSession();

            session.beginTransaction();

            student = session.get(Student.class, 1);

            session.getTransaction().commit();
        } finally {
            sessionFactory.close();
        }

        Assert.assertEquals("Milos", student.getFirstName());
        Assert.assertEquals("Tosic", student.getLastName());
        Assert.assertEquals("toske@mail.com", student.getEmail());
        Assert.assertEquals(1, student.getId());

    }

    @Ignore
    @Test
    public void testGetAllStudents() {

        List<Student> students;

        try {
            session = sessionFactory.getCurrentSession();

            session.beginTransaction();

            students = session.createQuery("from Student").getResultList();

            session.getTransaction().commit();
        } finally {
            sessionFactory.close();
        }

        Assert.assertEquals(5, students.size());

        students.forEach(student -> System.out.println(student));

    }

    @Ignore
    @Test
    public void testGetStudentsByCriteria() {
        List<Student> students;

        try {
            session = sessionFactory.getCurrentSession();

            session.beginTransaction();

            students = session.createQuery("from Student s where s.lastName='Tosic'").getResultList();

            session.getTransaction().commit();
        } finally {
            sessionFactory.close();
        }

        Assert.assertEquals(2, students.size());

        students.forEach(student -> System.out.println(student));
    }

    @Ignore
    @Test
    public void testUpdateStudent() {

        int studentId = 1;
        Student student;

        try {
            session = sessionFactory.getCurrentSession();

            session.beginTransaction();

            student = session.get(Student.class, studentId);

            student.setEmail("milos@mail.com");

            session.getTransaction().commit();
        } finally {
            sessionFactory.close();
        }

    }

    @Ignore
    @Test
    public void testUpdateStudentsWithSomeCriteria() {

        try {
            session = sessionFactory.getCurrentSession();

            session.beginTransaction();

            session.createQuery("update Student s set s.email='tosic@mail.com' where s.lastName='Tosic'").executeUpdate();

            session.getTransaction().commit();
        } finally {
            sessionFactory.close();
        }
    }

    @Ignore
    @Test
    public void testDeleteStudentFromDB() {

        Student student;
        List<Student> students;

        try {
            session = sessionFactory.getCurrentSession();
            session.beginTransaction();

            student = session.get(Student.class, 5);

            session.delete(student);

            students = session.createQuery("from Student ").getResultList();

            session.getTransaction().commit();
        } finally {
            sessionFactory.close();
        }

        Assert.assertEquals(4, students.size());

    }




}
