package com.toske.springaop;

import com.toske.springaop.config.ConfigDemo;
import com.toske.springaop.dao.AccountDAO;
import com.toske.springaop.dao.ProductDAO;
import com.toske.springaop.model.Account;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class SpringAopTest {
    
    private static final Log LOGGER = LogFactory.getLog(SpringAopTest.class);

    AnnotationConfigApplicationContext context;

    @Before
    public void init() {
        context = new AnnotationConfigApplicationContext(ConfigDemo.class);
    }

    @After
    public void after() {
        context.close();
    }

    @Test
    public void testBeforeAdvice() {

        AccountDAO accountDAO = context.getBean("accountDAO", AccountDAO.class);

        accountDAO.addAccount();

        LOGGER.info("\n Call method one more time: ");

        accountDAO.addAccount();


    }

    @Test
    public void testBeforeAdvice2() {

        AccountDAO accountDAO = context.getBean("accountDAO", AccountDAO.class);
        ProductDAO productDAO = context.getBean("productDAO", ProductDAO.class);

        accountDAO.addAccount();

        productDAO.addAccount();

    }

    @Test
    public void testAfterReturningAdvice() {

        AccountDAO accountDAO = context.getBean("accountDAO", AccountDAO.class);

        accountDAO.addAccount();

        List<Account> accounts = accountDAO.getAccounts();

        LOGGER.info("Printing method in Test...");
        /**
         * Be careful with using @AfterReturning advice
         * As you can see in this test, if you change returning result in AOP advice
         * it has impact on hole application
         */

        LOGGER.info(accounts);

    }
}
