package com.toske.springaop.aspect;

import com.toske.springaop.model.Account;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.List;

@Aspect
@Component
public class LoggingAspect {

    private final Log LOGGER = LogFactory.getLog(getClass());

    @Before("execution(public void addAccount())")
    public void beforeAddAccountAdvice() {
        LOGGER.info("\n=====>>> Executing @Before advice on addAccount()");
    }

    @AfterReturning(pointcut = "execution(* com.toske.springaop.dao.AccountDAO.getAccounts())",
    returning = "result")
    public void afterReturningGetAccounts(JoinPoint joinPoint, List<Account> result) {
        LOGGER.info(result);

        // change returned list and make names in all caps

        result.forEach(account -> account.setName(account.getName().toUpperCase()));

        LOGGER.info(result);
    }
}
