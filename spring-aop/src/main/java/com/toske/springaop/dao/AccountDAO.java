package com.toske.springaop.dao;

import com.toske.springaop.model.Account;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccountDAO {

    public void addAccount() {
        System.out.println(getClass() + ": DOING MY DB WORK: ADDING AN ACCOUNT");
    }

    public List<Account> getAccounts() {
        return List.of(new Account("Milos", "milos@mail.com"),
                new Account("Milena", "milena@mail.com"));
    }
}
