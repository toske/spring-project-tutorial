package com.toske.springaop.dao;

import org.springframework.stereotype.Component;

@Component
public class ProductDAO {

    public void addAccount() {


        System.out.println(getClass() + ": DOING ON MY DB WORK: ADDING ACCOUNT FOR PRODUCT");
    }
}
