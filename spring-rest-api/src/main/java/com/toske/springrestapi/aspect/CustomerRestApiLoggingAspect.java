package com.toske.springrestapi.aspect;

import com.toske.springrestapi.entity.Customer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Aspect
@Component
public class CustomerRestApiLoggingAspect {

    private static final Log LOGGER = LogFactory.getLog(CustomerRestApiLoggingAspect.class);

    @Pointcut("execution(public * com.toske.springrestapi.rest.customer.controller.CustomerRestController.getCustomers(..))")
    private void getCustomers() {}

    @Before("getCustomers()")
    public void beforeGetCustomers(JoinPoint joinPoint) {
        LOGGER.info("Client call getCustomers() method.\nTrying to get all customers from database...");
    }

    @AfterReturning(value = "getCustomers()", returning = "result")
    public void afterGetCustomers(JoinPoint joinPoint, List<Customer> result) {
        LOGGER.info("Sending all customers to the client... " + result.toString());
    }

    @AfterThrowing("getCustomers()")
    public void afterGetCustomersThrowingException() {
        LOGGER.error("Cannot send all customers to the client.");
    }

}
