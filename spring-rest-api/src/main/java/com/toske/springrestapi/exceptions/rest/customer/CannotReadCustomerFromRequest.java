package com.toske.springrestapi.exceptions.rest.customer;

public class CannotReadCustomerFromRequest extends RuntimeException {

    public CannotReadCustomerFromRequest() {
        super();
    }

    public CannotReadCustomerFromRequest(String message) {
        super(message);
    }

    public CannotReadCustomerFromRequest(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotReadCustomerFromRequest(Throwable cause) {
        super(cause);
    }

    protected CannotReadCustomerFromRequest(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
