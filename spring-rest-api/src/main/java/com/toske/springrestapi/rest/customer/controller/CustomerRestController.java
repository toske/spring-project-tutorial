package com.toske.springrestapi.rest.customer.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.toske.springrestapi.entity.Customer;
import com.toske.springrestapi.exceptions.rest.customer.CannotReadCustomerFromRequest;
import com.toske.springrestapi.exceptions.rest.customer.CustomerNotFoundException;
import com.toske.springrestapi.rest.customer.response.CustomerResponse;
import com.toske.springrestapi.rest.customer.response.success.CustomerSuccessResponse;
import com.toske.springrestapi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api")
public class CustomerRestController {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private CustomerService customerService;

    @GetMapping(value = "/customer", produces = "application/json")
    public List<Customer> getCustomers() {
        List<Customer> customers;

        try {
            customerService.getCustomers();
        } catch (Exception exception) {
            throw new RuntimeException(exception.getMessage());
        }

        return customerService.getCustomers();
    }

    @GetMapping(value = "/customer/{customerId}", produces = "application/json")
    public Customer getCustomer(@PathVariable int customerId) {

        Customer customer = customerService.getCustomer(customerId);

        if (Objects.isNull(customer)) {
            throw new CustomerNotFoundException("Customer id not found: " + customerId);
        }

        return customerService.getCustomer(customerId);
    }

    @PostMapping(value = "/customer", consumes = "application/json", produces = "application/json")
    public ResponseEntity<CustomerResponse> saveCustomer(@RequestBody String jsonString) {

        Customer customer = null;

        try {
            customer = MAPPER.readValue(jsonString, Customer.class);
        } catch (JsonProcessingException ex) {
            throw new CannotReadCustomerFromRequest("Cannot read this request as Customer: " + jsonString);
        }

        customerService.saveCustomer(customer);

        CustomerResponse response = new CustomerSuccessResponse(HttpStatus.OK.value(),
                "Customer successfully saved into database.",
                System.currentTimeMillis());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/customer/{customerId}")
    public ResponseEntity<CustomerResponse> deleteCustomer(@PathVariable int customerId) {
        int index = customerService.deleteCustomer(customerId);

        if (index == 0) {
            throw new CustomerNotFoundException("Customer with ID=" + customerId + " not founded in database.");
        }

        CustomerResponse response = new CustomerSuccessResponse(HttpStatus.OK.value(),
                "Customer successfully deleted.",
                System.currentTimeMillis());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
