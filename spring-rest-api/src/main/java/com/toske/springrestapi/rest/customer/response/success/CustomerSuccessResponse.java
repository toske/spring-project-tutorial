package com.toske.springrestapi.rest.customer.response.success;

import com.toske.springrestapi.rest.customer.response.CustomerResponse;

public class CustomerSuccessResponse extends CustomerResponse {

    public CustomerSuccessResponse(int status, String message, Long timestamp) {
        super(status, message, timestamp);
    }
}
