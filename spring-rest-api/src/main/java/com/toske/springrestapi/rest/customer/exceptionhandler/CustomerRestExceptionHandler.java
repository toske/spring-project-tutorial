package com.toske.springrestapi.rest.customer.exceptionhandler;

import com.toske.springrestapi.exceptions.rest.customer.CannotReadCustomerFromRequest;
import com.toske.springrestapi.exceptions.rest.customer.CustomerNotFoundException;
import com.toske.springrestapi.rest.customer.response.CustomerResponse;
import com.toske.springrestapi.rest.customer.response.error.CustomerErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class CustomerRestExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<CustomerResponse> handleException(CustomerNotFoundException ex) {
        CustomerResponse response = new CustomerErrorResponse(HttpStatus.NOT_FOUND.value(),
                                                                    ex.getMessage(),
                                                                    System.currentTimeMillis());

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<CustomerResponse> handleException(MethodArgumentTypeMismatchException ex) {
        CustomerResponse response = new CustomerErrorResponse(HttpStatus.BAD_REQUEST.value(),
                                                                    ex.getMessage(),
                                                                    System.currentTimeMillis());

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<CustomerResponse> handleException(NoHandlerFoundException ex) {
        CustomerResponse response = new CustomerErrorResponse(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(),
                System.currentTimeMillis());

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<CustomerResponse> handleException(Exception ex) {
        CustomerResponse response = new CustomerErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                ex.getMessage(),
                System.currentTimeMillis());

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<CustomerResponse> handleException(CannotReadCustomerFromRequest ex) {
        CustomerResponse response = new CustomerErrorResponse(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(),
                System.currentTimeMillis());

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


}
