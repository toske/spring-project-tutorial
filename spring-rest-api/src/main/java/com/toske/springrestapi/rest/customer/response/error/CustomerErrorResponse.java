package com.toske.springrestapi.rest.customer.response.error;


import com.toske.springrestapi.rest.customer.response.CustomerResponse;

public class CustomerErrorResponse extends CustomerResponse {

    public CustomerErrorResponse(int status, String message, Long timestamp) {
        super(status, message, timestamp);
    }
}
