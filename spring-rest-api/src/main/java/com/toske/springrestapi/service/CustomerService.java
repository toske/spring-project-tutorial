package com.toske.springrestapi.service;

import com.toske.springrestapi.entity.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getCustomers();
    Customer getCustomer(int id);
    void saveCustomer(Customer customer);
    Integer deleteCustomer(int id);

}
