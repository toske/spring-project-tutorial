<%--
  Created by IntelliJ IDEA.
  User: milos
  Date: 11/6/2020
  Time: 11:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>HelloWorld</title>
</head>
<body>
    <h2>HelloWorld from Spring MVC</h2>
    <p>Student name: ${param.studentName}</p>
</body>
</html>
