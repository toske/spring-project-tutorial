<%--
  Created by IntelliJ IDEA.
  User: milos
  Date: 11/8/2020
  Time: 7:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Student confirmation</title>
</head>
<body>
    <p>The confirmed student: ${student.firstName} ${student.lastName}</p>
    <p>Country: ${student.country}</p>
    <p>Programming language: ${student.favoriteLanguage}</p>
    <p>Operating systems:</p>
    <ul>
        <c:forEach var="item" items="${student.operatingSystems}">
            <li>${item}</li>
        </c:forEach>
    </ul>
</body>
</html>
