<%--
  Created by IntelliJ IDEA.
  User: milos
  Date: 11/8/2020
  Time: 8:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Customer Confirmation</title>
</head>
<body>
    <p>The customer confirmed: ${customer.firstName} ${customer.lastName}</p>
    <p>Free passes: ${customer.freePasses}</p>
    <p>Postal code: ${customer.postalCode}</p>
    <p>Course code: ${customer.crsCode}</p>
</body>
</html>
