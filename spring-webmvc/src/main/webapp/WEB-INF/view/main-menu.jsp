<%--
  Created by IntelliJ IDEA.
  User: milos
  Date: 11/6/2020
  Time: 9:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home page</title>
</head>
<body>
    <h2>Main menu</h2>
    <hr>
    <a href="/showForm">HelloWorld form</a>
    <br><br>
    <a href="/showFormVersionTwo">HelloWorld form with Model</a>
    <br><br>
    <a href="/student/showForm">Student form</a>
    <br><br>
    <a href="/customer/showForm">Customer form</a>
</body>
</html>
