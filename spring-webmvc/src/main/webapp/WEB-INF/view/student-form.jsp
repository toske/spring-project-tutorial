<%--
  Created by IntelliJ IDEA.
  User: milos
  Date: 11/8/2020
  Time: 7:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Student form</title>
</head>
<body>
    <h2>Student form</h2>
    <form:form action="processForm" modelAttribute="student">
        First name: <form:input path="firstName"/>
        <br><br>
        Last name: <form:input path="lastName"/>
        <br><br>
        Country: <form:select path="country">
                    <form:option value="United States of America" label="USA"/>
                    <form:option value="Great Britania" label="GB"/>
                    <form:option value="Serbia" label="SRB"/>
                    <form:option value="Switzerland" label="CH"/>
                 </form:select>
        <br><br>
        Favorite programming language:
        <br>
        <form:radiobutton path="favoriteLanguage" value="Java"/> Java<br/>
        <form:radiobutton path="favoriteLanguage" value="PHP"/> PHP<br/>
        <form:radiobutton path="favoriteLanguage" value="Python"/> Python<br/>
        <form:radiobutton path="favoriteLanguage" value="C#"/> C#
        <br><br>
        Operating Systems:
        <br>
        <form:checkbox path="operatingSystems" value="Linux"/> Linux <br/>
        <form:checkbox path="operatingSystems" value="Windows"/> Windows <br/>
        <form:checkbox path="operatingSystems" value="Mac OS X"/> Mac OS X
        <br><br>
        <input type="submit" value="Submit"/>
    </form:form>

</body>
</html>
