package com.toske.springwebmvc.customvalidation;

import com.toske.springwebmvc.customvalidation.constraintvalidator.CourseCodeConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CourseCodeConstraintValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CourseCode {

    public String value() default "CCODE";

    public String message() default "must start with CCODE";

    /**
     * If not define this there is exception when try to run on server
     */
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
