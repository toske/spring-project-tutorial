package com.toske.springwebmvc.customvalidation.constraintvalidator;

import com.toske.springwebmvc.customvalidation.CourseCode;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CourseCodeConstraintValidator implements ConstraintValidator<CourseCode, String> {

    private String coursePrefix;

    @Override
    public void initialize(CourseCode courseCode) {
        coursePrefix = courseCode.value();
    }

    @Override
    public boolean isValid(String theCode, ConstraintValidatorContext constraintValidatorContext) {
       if (theCode != null) {
           return theCode.startsWith(getCoursePrefix());
       }
       return true;
    }

    private String getCoursePrefix() {
        return coursePrefix;
    }

}
