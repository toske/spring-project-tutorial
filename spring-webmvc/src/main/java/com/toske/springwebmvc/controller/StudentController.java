package com.toske.springwebmvc.controller;

import com.toske.springwebmvc.model.Student;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
public class StudentController {

    private static final Log LOGGER = LogFactory.getLog(StudentController.class);

    @RequestMapping("/showForm")
    public String showForm(Model theModel) {
        theModel.addAttribute("student", new Student());

        return "student-form";
    }

    @RequestMapping("/processForm")
    public String processForm(@ModelAttribute("student") Student theStudent) {
        LOGGER.info("Student: " + theStudent.getFirstName() + " " + theStudent.getLastName() + " " + theStudent.getCountry());
        return "student-confirmation";
    }

}
