package com.toske.springwebapp.aspect;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CRMLoggingAspect {

    private static final Log LOGGER = LogFactory.getLog(CRMLoggingAspect.class);

    @Pointcut("execution(* com.toske.springwebapp.controller.*.*(..))")
    private void forControllerPackage() {}

    @Pointcut("execution(* com.toske.springwebapp.service.*.*(..))")
    private void forServicePackage() {}

    @Pointcut("execution(* com.toske.springwebapp.dao.*.*(..))")
    private void forDAOPackage() {}

    @Pointcut("forControllerPackage() || forServicePackage() || forDAOPackage()")
    private void forAppFlow() {}

    @Before("forAppFlow()")
    public void before(JoinPoint joinPoint) {
        String method = joinPoint.getSignature().toShortString();
        LOGGER.info("====>>> in @Before : calling method: " + method);

        Object[] args = joinPoint.getArgs();

        for (Object arg : args) {
            LOGGER.info("====>>> argument: " + arg);
        }
    }

    @AfterReturning(pointcut = "forAppFlow()", returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {
        String method = joinPoint.getSignature().toShortString();
        LOGGER.info("====>>> in @AfterReturning : calling method: " + method);

        LOGGER.info("====>>> result: " + result);
    }

}
