package com.toske.springsecurity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/leaders")
public class LeaderController {

    @GetMapping
    public String leaders() {
        return "leaders";
    }
}
