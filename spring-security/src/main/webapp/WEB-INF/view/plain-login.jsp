<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: milos
  Date: 11/24/2020
  Time: 11:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Plain login page</title>
    <style>
        .error {
            color: darkred;
        }
    </style>
</head>
<body>
        <h2>Login form</h2>
        <form:form action="${pageContext.request.contextPath}/authenticateTheUser" method="post">
            <c:if test="${param.error != null}">
                <i class="error">You entered invalid username/password</i>
            </c:if><br><br>
           User name: <input type="text" name="username"/> <br><br>
            Password: <input type="password" name="password"/> <br><br>
            <input type="submit" value="Log in"/>
        </form:form>
</body>
</html>
