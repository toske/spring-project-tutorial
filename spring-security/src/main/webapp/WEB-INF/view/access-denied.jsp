<%--
  Created by IntelliJ IDEA.
  User: milos
  Date: 11/25/2020
  Time: 9:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Forbidden access</title>
</head>
<body>
    <br>
    <br>
    <hr>
    <h2>You don't have permission to access this source!</h2>
    <hr>
    <p><a href="${pageContext.request.contextPath}/">Go back</a></p>
</body>
</html>
