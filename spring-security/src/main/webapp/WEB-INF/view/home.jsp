<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: milos
  Date: 11/24/2020
  Time: 10:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home page</title>
</head>
<body>
    <h2>Our company home page</h2>
    <hr>
    <h4>User : <security:authentication property="principal.username"/></h4>
    <h4>Role: <security:authentication property="principal.authorities"/></h4>
    <hr>
    <h4>Welcome to the Our company home page!</h4>
    <hr>
    <security:authorize access="hasRole('MANAGER')">
    <p><a href="${pageContext.request.contextPath}/leaders">Leadership meeting</a>
        (Only for managers)</p>
    </security:authorize>
    <security:authorize access="hasRole('ADMIN')">
    <p><a href="${pageContext.request.contextPath}/systems">IT Systems meeting</a>
        (Only for admins)</p>
    </security:authorize>
    <hr>
    <form:form action="${pageContext.request.contextPath}/logout" method="post">
        <input type="submit" value="Log out"/>
    </form:form>
</body>
</html>
