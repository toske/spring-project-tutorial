package com.toske.springthree;

import com.toske.springthree.configuration.AppConfig;
import com.toske.springthree.model.BasketballPlayer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class CoachTest {

    private AnnotationConfigApplicationContext context;

    @Before
    public void init() {
        context = new AnnotationConfigApplicationContext(AppConfig.class);
    }

    @Test
    public void testCreatingBean() {

        Coach coach = context.getBean("baseballCoach", Coach.class);

        Assert.assertEquals("Hit ball every day!", coach.getDailyWorkout());
        Assert.assertEquals("This is your lucky day!", coach.getDailyFortune());

        context.close();
    }

    @Test
    public void testInjectLiteralValues() {
        TrackCoach coach = context.getBean("trackCoach", TrackCoach.class);

        Assert.assertEquals("Mike", coach.getName());
        Assert.assertEquals("coach@sietle.com", coach.getEmail());

        context.close();
    }

    @Test
    public void testBeanAnnotation() {
        BasketballPlayer player = context.getBean("basketballPlayer", BasketballPlayer.class);

        System.out.println(player.getFirstName());
        System.out.println(player.getLastName());
        System.out.println(player.getTeamName());

        context.close();
    }
}
