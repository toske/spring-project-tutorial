package com.toske.springthree.model;

public class BasketballPlayer {

    private String firstName;
    private String lastName;
    private Team team;

    public BasketballPlayer(String firstName, String lastName, Team team) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getTeamName() {
        return team.getTeamName();
    }
}
