package com.toske.springthree;

public interface FortuneService {
    String getFortune();
}
