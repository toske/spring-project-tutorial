package com.toske.springthree;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortune();
}
