package com.toske.springthree.configuration;

import com.toske.springthree.model.BasketballPlayer;
import com.toske.springthree.model.Team;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "com.toske.springthree")
@PropertySource("data.properties")
public class AppConfig {

    @Bean
    public Team team() {
        return new Team("Radnicki");
    }

    @Bean
    public BasketballPlayer basketballPlayer() {
        return new BasketballPlayer("Milos", "Tosic", team());
    }


}
