package com.toske.springone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CoachTest {

    private static final Log LOGGER = LogFactory.getLog(CoachTest.class);

    private ClassPathXmlApplicationContext context;

    @Before
    public void init() {
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }

    @Test
    public void testCreatingBaseballCoach() {
        // retrieve bean from spring container
        Coach theCoach = context.getBean("myCoach", Coach.class);
        // call methods of the bean
        LOGGER.info(theCoach.getDailyWorkout());
        LOGGER.info(theCoach.getDailyFortune());
        Assert.assertEquals("Hit the ball for Home run every day!", theCoach.getDailyWorkout());
        Assert.assertEquals("Today is your lucky day!", theCoach.getDailyFortune());

        // close the context
        context.close();
    }

    @Test
    public void testSetterInjection() {

        Coach theCoach = context.getBean("myCricketCoach", Coach.class);

        System.out.println(theCoach.getDailyWorkout());
        System.out.println(theCoach.getDailyFortune());

        context.close();
    }

    @Test
    public void testInjectLiteralValuesHardCoded() {

        CricketCoach theCoach = context.getBean("myCricketCoach", CricketCoach.class);

        System.out.println(theCoach.getDailyWorkout());
        System.out.println(theCoach.getDailyFortune());
        System.out.println(theCoach.getTeam());
        System.out.println(theCoach.getEmailAddress());

        Assert.assertEquals("Radnicki", theCoach.getTeam());
        Assert.assertEquals("coach@radnicki.com", theCoach.getEmailAddress());

        context.close();
    }

    @Test
    public void testInjectLiteralValuesFromPropertiesFile() {

        Coach theCoach = context.getBean("myFootballCoach", Coach.class);

        System.out.println(theCoach.getDailyWorkout());
        System.out.println(theCoach.getDailyFortune());
        System.out.println(((FootballCoach) theCoach).getTeam());
        System.out.println(((FootballCoach) theCoach).getEmailAddress());

        Assert.assertEquals("Slavija", ((FootballCoach) theCoach).getTeam());
        Assert.assertEquals("coach@slavija.com", ((FootballCoach) theCoach).getEmailAddress());

        context.close();

    }

    @Test
    public void testSingletonScope() {
        Coach theCoach = context.getBean("myBasketballCoach", Coach.class);
        Coach anotherCoach = context.getBean("myBasketballCoach", Coach.class);

        System.out.println(((BasketballCoach) theCoach).getName());
        System.out.println(((BasketballCoach) anotherCoach).getName());

        Assert.assertEquals(((BasketballCoach) theCoach).getName(), ((BasketballCoach) anotherCoach).getName());
        Assert.assertEquals(theCoach, anotherCoach);

        ((BasketballCoach) theCoach).setName("Harvey");

        System.out.println(((BasketballCoach) theCoach).getName());
        System.out.println(((BasketballCoach) anotherCoach).getName());

        Assert.assertEquals(((BasketballCoach) theCoach).getName(), ((BasketballCoach) anotherCoach).getName());
        Assert.assertEquals(theCoach.toString(), anotherCoach.toString());

        Assert.assertEquals(theCoach, anotherCoach);

        context.close();
    }

    @Test
    public void testPrototypeScope() {
        SwimCoach theCoach = context.getBean("mySwimCoach", SwimCoach.class);
        SwimCoach anotherCoach = context.getBean("mySwimCoach", SwimCoach.class);

        Assert.assertNotEquals(theCoach, anotherCoach);
        Assert.assertEquals(theCoach.getName(), anotherCoach.getName());

        System.out.println(theCoach.getName());
        System.out.println(anotherCoach.getName());

        theCoach.setName("Mirko");

        Assert.assertNotEquals(theCoach.getName(), anotherCoach.getName());

        System.out.println(theCoach.getName());
        System.out.println(anotherCoach.getName());

        context.close();
    }

    @Test
    public void testBeanLifecycle() {

        context = new ClassPathXmlApplicationContext("lifecycle.xml");

        TrackCoach theCoach = context.getBean("myTrackCoach", TrackCoach.class);

        System.out.println(theCoach.getDailyWorkout());
        System.out.println(theCoach.getDailyFortune());

        context.close();

    }
}
