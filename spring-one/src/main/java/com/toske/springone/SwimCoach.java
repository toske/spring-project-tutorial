package com.toske.springone;

public class SwimCoach implements Coach {

    private FortuneService fortuneService;
    private String name;

    public SwimCoach(FortuneService fortuneService, String name) {
        this.fortuneService = fortuneService;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getDailyWorkout() {
        return "Swim every they for 3km!";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }
}
