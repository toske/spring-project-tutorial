package com.toske.springone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TrackCoach implements Coach {

    private static final Log LOGGER = LogFactory.getLog(TrackCoach.class);

    private FortuneService fortuneService;

    public TrackCoach(FortuneService fortuneService) {
        LOGGER.info("TrackCoach: inside constructor!");
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "Run every day for 5km!";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

    public void initMethod() {
        LOGGER.info("TrackCoach: init method run!");
    }

    public void destroyMethod() {
        LOGGER.info("TrackCoach: destroy method run!");
    }
}
