package com.toske.springone;

public interface Coach {

    String getDailyWorkout();
    String getDailyFortune();
}
