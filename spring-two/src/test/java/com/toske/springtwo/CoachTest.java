package com.toske.springtwo;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CoachTest {

    @Test
    public void testAnnotationCreatingBean() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        Coach theCoach = context.getBean("baseballCoach", Coach.class);

        System.out.println(theCoach.getDailyWorkout());
        System.out.println(theCoach.getDailyFortune());

        context.close();
    }

    @Test
    public void testAnnotationCreatingBeanQualifier() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        Coach theCoach = context.getBean("trackCoach", Coach.class);

        System.out.println(theCoach.getDailyWorkout());
        System.out.println(theCoach.getDailyFortune());

        context.close();
    }

    @Test
    public void testSingleton() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        TennisCoach coach1 = context.getBean("tennisCoach", TennisCoach.class);
        TennisCoach coach2 = context.getBean("tennisCoach", TennisCoach.class);

        Assert.assertEquals(coach1, coach2);

        coach1.setName("Harvey");

        Assert.assertEquals(coach1, coach2);
        Assert.assertEquals(coach1.getName(), coach2.getName());

        context.close();
    }

    @Test
    public void testPrototype() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        BasketballCoach coach1 = context.getBean("basketballCoach", BasketballCoach.class);
        BasketballCoach coach2 = context.getBean("basketballCoach", BasketballCoach.class);

        Assert.assertNotEquals(coach1, coach2);

        coach1.setName("Mike");

        Assert.assertNotEquals(coach1, coach2);
        Assert.assertNotEquals(coach1.getName(), coach2.getName());

        context.close();
    }

    @Test
    public void testLifecycleOfBean() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        context.close();
    }

    @Test
    public void testInjectLiteralValues() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        FootballCoach coach = context.getBean("footballCoach", FootballCoach.class);

        Assert.assertEquals("Harvey", coach.getName());
        Assert.assertEquals("coach@nyc.com", coach.getEmail());

        context.close();
    }
}
