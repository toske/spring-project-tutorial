package com.toske.springtwo;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class RandomFortuneService implements FortuneService {

    private static final String[] fortunes = {"This is good day!",
                                    "Better stay at home today!",
                                    "Maybe it will be rain, maybe it will be sunshine!"};

    @Override
    public String getFortune() {
        Random random = new Random();
        int index = random.nextInt(fortunes.length);
        return fortunes[index];
    }
}
