package com.toske.springtwo;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortune();
}
