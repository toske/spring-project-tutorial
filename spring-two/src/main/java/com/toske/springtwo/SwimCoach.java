package com.toske.springtwo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class SwimCoach implements Coach {

    private static final Log LOGGER = LogFactory.getLog(SwimCoach.class);

    private FortuneService fortuneService;

    public SwimCoach() {
        LOGGER.info("SwimCoach: inside constructor!");
    }

    @Autowired
    @Qualifier("randomFortuneService")
    public void setFortuneService(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @PostConstruct
    public void initMethod() {
        LOGGER.info("SwimCoach: inside init method!");
    }

    @PreDestroy
    public void destroyMethod() {
        LOGGER.info("SwimCoach: inside destroy method!");
    }

    @Override
    public String getDailyWorkout() {
        return null;
    }

    @Override
    public String getDailyFortune() {
        return null;
    }
}
