package com.toske.springtwo;

public interface FortuneService {

    String getFortune();
}
